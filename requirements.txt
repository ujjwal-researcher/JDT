torch==1.7.1
torchvision==0.8.2
torchsummary==1.5.1
torchmetrics==0.5.1
pre-commit==2.15.0
